<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Website Title
 *
 * Write the name of your website this will appear by default.
*/
$config['ProjectName'] = 'FixCore';

/**
 * Maintenance Mode
 * TRUE = Enable
 * FALSE = Disable
*/
$config['ProjectMaintenance'] = TRUE;

/**
 * Theme Name
 *
 * Write the name of your theme
 * The name is the same as the main folder
 * The css must also have the same name
 * Default: default
*/
$config['ProjectTheme'] = 'default';

/* 
 * Enable https forced
*/
$config['ProjectSSLForced'] = false;
